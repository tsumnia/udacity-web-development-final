import os
import webapp2
import jinja2
import json
import random, string, hashlib
import re
import logging

from time import time, sleep
from google.appengine.api import memcache
from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(autoescape=False,
							   loader=jinja2.FileSystemLoader(template_dir))

def valid_username(username):
    if username:
        USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
        return not USER_RE.match(username) is None
    return False

def valid_password(password):
    if password:
        PSWD_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
        return not PSWD_RE.match(password) is None
    return False

def verify_password(password, verify):
    return password == verify and verify != ''

def valid_email(email):
    if email:
        EMAIL_RE = re.compile(r"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")
        return not EMAIL_RE.match(email) is None
    return False

## Salting w/ SHA-256
def make_salt():
    return ''.join(random.choice(string.letters) for x in xrange(5))

def make_pw_hash(name, pw, salt=None):
	if not salt:
		salt = make_salt()
	hashed = hashlib.sha256(name + pw + salt).hexdigest()
	return hashed + ',' + salt

def valid_pw(name, pw):
	user_account = db.GqlQuery("SELECT * FROM User WHERE username = :name", name=name).get()
	salt = user_account.password.split(',')[1]
	hashed_pw = make_pw_hash(name, pw, salt)
	return hashed_pw == user_account.password

class Handler(webapp2.RequestHandler):
	def write(self, *a, **kw):
		self.response.out.write(*a, **kw)

	def render_str(self, template, **params):
		t = jinja_env.get_template(template)
		return t.render(params)

	def render(self, template, **kw):
		self.write(self.render_str(template, **kw))

class User(db.Model):
	username = db.StringProperty(required = True)
	password = db.StringProperty(required = True)
	email = db.StringProperty()
	created = db.DateTimeProperty(auto_now_add = True)

class Wiki(db.Model):
	subject = db.StringProperty(required = True)
	content = db.TextProperty(required = True)
	created = db.DateTimeProperty(auto_now_add = True)

class EditPage(Handler):
    def render_front(self, subject="", content="", error=""):
        self.render("new_wiki_page.html", subject=subject, content=content, error=error)

    def get(self, article_id):
        user = self.request.cookies.get('username')
        if user and user != '':
            wiki_article = memcache.get(article_id)
            if wiki_article:
                last_cache = int(time() - wiki_article[1])
                wiki_article = wiki_article[0]
            else:
                logging.error("DB QUERY")
                wiki_article = db.GqlQuery("SELECT * FROM Wiki WHERE subject = :subject", subject=article_id).get()
                last_cache = 0
                memcache.set(article_id, (wiki_article, time()))
                cache_time = 0
            if wiki_article:
                self.render_front(content=wiki_article.content)
            else:
                self.render_front()
        else:
            self.redirect("/signup")

    def post(self, article_id):
        content = self.request.get("content")

        if content:
            if article_id == '': article_id = 'home'
            b = Wiki(subject = article_id, content = content)
            b.put()
            memcache.set(article_id, (b, time()))
            sleep(1)
            if article_id != 'home': self.redirect("/%s" % article_id)
            else: self.redirect("/")
        else:
            error = "Please include a subject and content!"
            self.render_front(subject, content, error)

class WikiPage(Handler):
    def get(self, article_id):
        version = self.request("v")
        if version == '': 
            hist_index = -1
        else:
            hist_index = int(version)
        wiki_article = memcache.get(article_id)
        user = self.request.cookies.get('username')
        if wiki_article:
            last_cache = int(time() - wiki_article[hist_index][1])
            wiki_article = wiki_article[hist_index][0]
        else:
            logging.error("DB QUERY")
            wiki_article = db.GqlQuery("SELECT * FROM Wiki WHERE subject = :subject", subject=article_id).get()
            last_cache = 0
            memcache.set(article_id, (wiki_article, time()))
            wiki_article = wiki_article[hist_index][0]

        if wiki_article:
            self.render("wikiarticle.html", article = wiki_article, 
                                            article_id = article_id,
                                            cached = last_cache,
                                            user = user)
        else:
            if user and user != '':
                self.redirect('/_edit/%s' % article_id)
            else:
                self.redirect('/login')

class MainPage(Handler):
    def get(self, ):
        wiki_article = memcache.get('home')
        user = self.request.cookies.get('username')
        if wiki_article:
            last_cache = int(time() - wiki_article[1])
            wiki_article = wiki_article[0]
        else:
            logging.error("DB QUERY")
            wiki_article = db.GqlQuery("SELECT * FROM Wiki WHERE subject = :subject", subject='home').get()
            last_cache = 0
            memcache.set('home', (wiki_article, time()))
            cache_time = 0

        if wiki_article:
            self.render("wikiarticle.html", article = wiki_article, 
                                            article_id = 'home',
                                            cached = last_cache,
                                            user = user)
        else:
            if user and user != '':
                self.redirect('/_edit/')
            else:
                self.redirect('/login')

class Login(Handler):
    def write_form(self, username="", password="", invalid=""):
        self.render("login.html", username=username,
        							password=password,
        							invalid=invalid,
        							)

    def get(self):
        # Defaults Content-Type to HTML
        #self.response.headers['Content-Type'] = 'text/plain'
        self.write_form()

    def post(self):
        user_name = self.request.get('username')
        user_password = self.request.get('password')

        u = valid_username(user_name)
        v = valid_pw(user_name, user_password)

        if u and v:
            self.response.headers.add_header('Set-Cookie', str('username=%s;Path=/' % user_name))
            self.redirect("/")
        else:
        	self.write_form(invalid = "Invalid Login",
        					username = user_name)

class SignUp(Handler):
    def write_form(self, error_un="", error_ps="", error_ve="", error_em="",
                    username="", password="", verify="", email=""):
        self.render("sign_up.html", error_un=error_un,
        							error_ps=error_ps,
        							error_ve=error_ve,
        							error_em=error_em,
        							username=username,
        							password=password,
        							verify=verify,
        							email=email
        							)

    def get(self):
        # Defaults Content-Type to HTML
        #self.response.headers['Content-Type'] = 'text/plain'
        self.write_form()

    def post(self):
        user_name = self.request.get('username')
        user_password = self.request.get('password')
        user_verify = self.request.get('verify')
        user_email = self.request.get('email')

        u = valid_username(user_name)
        p = valid_password(user_password)
        v = verify_password(user_password, user_verify)
        e = valid_email(user_email)

        if u and p and v:
            if e or user_email == '':
            	hash_password = make_pw_hash(user_name, user_password)
            	user = User(username = user_name, password = hash_password, email = user_email)
            	user.put()
            	self.response.headers.add_header('Set-Cookie', str('username=%s;Path=/' % user_name))


                self.redirect("/")
            else:
                self.write_form(error_em = "Invalid Email",
                                username = user_name,
                                email = user_email)
        else:
            er_u, er_p, er_v = '', '', ''
            if not u:
                er_u = "That's not a valid username."
            if not p:
                er_p = "That wasn't a valid password."
            if not v:
                er_p = ''
                er_v = "You're passwords didn't match."
            self.render("sign_up.html", error_un=er_u,
            							error_ps=er_p,
                                        error_ve=er_v,
                                        error_em='',
                                        username='',
                                        password='',
                                        verify='',
                                        email=user_email
                                        )

class Logout(Handler):
    def get(self):
        # Defaults Content-Type to HTML
        #self.response.headers['Content-Type'] = 'text/plain'
        ## This is how you delete a cookie. Webapp2 actually has a delete cookie method that does exactly this too
        self.response.headers.add_header('Set-Cookie', str('username=;Path=/'))
        self.redirect('/signup')

class Flush(Handler):
    def get(self):
        memcache.flush_all()
        #self.redirect('/')

class HistoryPage(Handler):
    pass

PAGE_RE = r'((?:[a-zA-Z0-9_-]+/?)*)'
app = webapp2.WSGIApplication([('/', MainPage),
                               ('/signup', SignUp),
                               ('/login', Login),
                               ('/logout', Logout),
                               ('/_history/' + PAGE_RE, HistoryPage),
                               ('/_edit/' + PAGE_RE, EditPage),
                               ('/flush', Flush),
                               ('/'+PAGE_RE, WikiPage),
                               ],
                              debug=True)
